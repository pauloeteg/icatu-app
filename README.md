**Front end desenvolvido para plataforma de gerenciamento de schedules da Icatu.**

----------------------------------------------------------------------------------------

**Especificações técnicas:**

- Projeto construído com create-react-app.
- Utilizando sass e bootstrap para estilização.
- Utilizando react hook form para manipular formulários.
 
 

----------------------------------------------------------------------------------------

**Scripts:**

   **_yarn start_:** Inicializa o projeto.

**Mais informações:**
Por padrão o projeto subirá na porta 3000
