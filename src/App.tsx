import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./routes";
import { ToastContainer } from "react-toastify";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

import "./global.scss";

function App() {
  return (
    <DndProvider backend={HTML5Backend}>
      <ToastContainer
        autoClose={2500}
        closeButton={true}
        position="top-right"
      />
      <Router>
        <Routes />
      </Router>
    </DndProvider>
  );
}

export default App;
