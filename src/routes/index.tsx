import React from "react";
import { Route, Switch } from "react-router-dom";
import JobsPage from "../pages/Jobs";
import LayoutsPage from "../pages/Layouts";
import LayoutDetailPage from "../pages/ShowLayout";

import Login from "../pages/Login";

const PrivateRoutes: React.FC = () => {
  return (
    <Switch>
      <Route path="/" exact component={Login} />
      <Route path="/jobs" exact component={JobsPage} />
      <Route path="/layouts" exact component={LayoutsPage} />
      <Route path="/layouts/:id" component={LayoutDetailPage} />
    </Switch>
  );
};

export default PrivateRoutes;
