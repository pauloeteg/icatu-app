import { IJob } from "../models/IJob";
import api from "./api";

export const getJobs = async (): Promise<IJob[]> => {
  return (await api.get<IJob[]>("jobs"))?.data;
};

export const getOneJob = async (id: string | undefined): Promise<IJob> => {
  return (await api.get<IJob>(`jobs/${id}`))?.data;
};

export const deleteJob = async (id: string | undefined): Promise<IJob[]> => {
  return await api.delete(`jobs/${id}`);
};

export const createJob = async (job: any) => {
  return (await api.post("jobs", job))?.data;
};

export const updateJob = async (job: any) => {
  return (await api.put(`jobs/${job.id}`, job))?.data;
};
