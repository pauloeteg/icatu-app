import axios from "axios";
import Cookies from "js-cookie";
import { Alert } from "../components/Alert";

const api = axios.create({
  baseURL: `http://localhost:5000/`,
  headers: {
    "Content-Type": "application/json;charset=UTF-8",
  },
});

api.interceptors.request.use(async (config) => {
  const cookiesAuthentication = Cookies.get("access_token");
  if (cookiesAuthentication && config.headers) {
    config.headers.Authorization = `Bearer ${cookiesAuthentication}`;
  }

  return config;
});

api.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    const originalConfig = error.config;

    if (error?.response?.status === 401 && !originalConfig._retry) {
      const originalConfig = error.config;

      if (originalConfig.url !== "/" && error?.response) {
        if (
          error?.response?.status === 401 &&
          !originalConfig._retry &&
          error?.response?.data?.message !== "Credenciais inválidas"
        ) {
          originalConfig._retry = true;

          Alert.warning("Por favor, efetue login novamente.", undefined, () => {
            window.location.replace("/");
          });
          Cookies.remove("access_token");
        }

        return Promise.reject(error);
      }
    } else {
      Alert.error(error?.response?.data?.message);
    }
    return Promise.reject(error);
  }
);

export default api;
