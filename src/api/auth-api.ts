import Cookies from "js-cookie";
import api from "./api";

interface ILogin {
  email: string;
  password: string;
}

interface ITokenUserAuthenticated {
  access_token: string;
}

export const login = async (user: ILogin) => {
  const token = (await api.post<ITokenUserAuthenticated>("auth/login", user))
    ?.data;
  Cookies.set("access_token", token.access_token);
};
