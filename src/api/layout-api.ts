import { ILayout, IJson } from "../models/ILayout";
import api from "./api";

export const getLayouts = async (): Promise<ILayout[]> => {
  return (await api.get<ILayout[]>("layouts"))?.data;
};

export const getOneLayout = async (
  id: string | undefined
): Promise<ILayout> => {
  return (await api.get<ILayout>(`layouts/${id}`))?.data;
};

export const updateNameLayout = async (
  id: string | undefined,
  name: string | undefined
) => {
  return (await api.patch(`layouts/update-name/${id}`, { id, name }))?.data;
};

export const deleteLayout = async (
  id: string | undefined
): Promise<ILayout[]> => {
  return await api.delete(`layouts/${id}`);
};

export const deleteField = async (
  idLayout: string | undefined,
  posField: string | undefined
): Promise<ILayout[]> => {
  return await api.put(`layouts/delete-field/${idLayout}`, { posField });
};

export const createLayout = async (layout: ILayout) => {
  return (await api.post<ILayout>("layouts", layout))?.data;
};

export const duplicateLayout = async (id: string | undefined) => {
  if (id) {
    return (await api.post<ILayout>("layouts/duplicate-layout", { id }))?.data;
  }
};

export const updateLayout = async (layout: ILayout) => {
  return (await api.put(`layouts/${layout.id}`, layout))?.data;
};

export const updateField = async (
  idLayout: string | undefined,
  posField: number | undefined,
  field: IJson | undefined
) => {
  return (
    await api.patch(`layouts/update-field`, {
      idLayout,
      posField,
      field,
    })
  )?.data;
};

export const updatePositionField = async (
  idLayout: string | undefined,
  from: number | undefined,
  to: number | undefined
) => {
  return (
    await api.patch(`layouts/move-field`, {
      idLayout,
      from,
      to,
    })
  )?.data;
};

export const insertField = async (
  id: string | undefined,
  layoutJson: IJson
) => {
  if (id && layoutJson) {
    return (await api.put(`layouts/insert-field/${id}`, layoutJson))?.data;
  }
};
