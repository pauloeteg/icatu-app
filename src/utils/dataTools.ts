import { format, parseISO } from "date-fns";

export class DataTools {
  static euaToBr = (date: string | undefined) => {
    if (date) {
      return format(parseISO(date), "dd-MM-yyyy");
    }
  };

  static addStrHrs = (hrs: string | number | undefined) => {
    if (hrs) {
      return hrs + ":00" + (Number(hrs) !== 1 ? " hrs" : " hr");
    }
  };
}
