import React, { useEffect, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { SubmitHandler, useForm } from "react-hook-form";
import Select from "react-select";
import { updateField } from "../../../api/layout-api";

import { Alert } from "../../Alert";

import {
  typeBooleanOptions,
  typeFieldOptions,
  IJson,
  typeFieldBooleanOptions,
  formatDateHourOptions,
  TypeFieldEnum,
} from "../../../models/ILayout";

import "../styles.scss";
import { InputsFieldProps } from "../../../models/IInput";

interface IModal {
  show: boolean;
  handleClose: () => void;
  updatePage: () => void;
  posField: number | undefined;
  idLayout: string | undefined;
  field: IJson | undefined;
}

const ModalEditField: React.FC<IModal> = ({
  show,
  handleClose,
  updatePage,
  posField,
  idLayout,
  field,
}) => {
  const [fieldMandatory, setFieldMandatory] = useState<undefined | string>(
    undefined
  );
  const [fieldType, setFieldType] = useState<undefined | string>(undefined);
  const [fieldFixed, setFieldFixed] = useState<undefined | string>(undefined);
  const [typeField, setTypeField] = useState<undefined | TypeFieldEnum>(
    undefined
  );
  const [formatDate, setFieldFormatDate] = useState<undefined | string>(
    undefined
  );

  const onSubmit: SubmitHandler<InputsFieldProps> = async (data) => {
    try {
      const fieldUpdated: IJson = {
        fixed: fieldFixed,
        mandatory: fieldMandatory,
        type: fieldType,
        typeField: typeField,
        name: data.inputNome,
        default: data.inputPadrao,
        size: data.inputTamanho,
        customFieldNumber: data.inputCustomFieldNumber,
        fillerSize: data.inputTamanhoFiller,
        sequencialSize: data.inputTamanhoSequencial,
        formatDate: formatDate,
      };
      await updateField(idLayout, posField, fieldUpdated);

      Alert.success(
        "Field atualizado com sucesso.",
        cleanAndCloseModal,
        undefined
      );
    } catch (error: any) {
      return;
    }
  };

  const { register, handleSubmit, reset } = useForm();

  const cleanAndCloseModal = () => {
    reset();
    handleClose();
    updatePage();
  };

  useEffect(() => {
    setFieldFixed(field?.fixed);
    setFieldType(field?.type);
    setFieldMandatory(field?.mandatory);
    setTypeField(field?.typeField);
  }, [field]);

  return (
    <Modal
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      animation={false}
    >
      <Modal.Header>
        <Modal.Title>Editar Campo</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className="mb-3" controlId="formNome">
            <Form.Label>Nome: </Form.Label>
            <Form.Control
              type="text"
              defaultValue={field?.name}
              {...register("inputNome", { required: true })}
              maxLength={30}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formObrigatorio">
            <Form.Label>Qual tipo do campo?</Form.Label>
            <Select
              options={typeFieldBooleanOptions}
              placeholder="Clique para selecionar"
              onChange={(e) => setTypeField(e?.value)}
              defaultValue={{
                label: field?.typeField,
                value: field?.typeField,
              }}
            />
          </Form.Group>

          {typeField === TypeFieldEnum.SEQUENCIAL && (
            <Form.Group className="mb-3" controlId="formNome">
              <Form.Label title="Example: (1 - 1), (2 - 01), (3 - 001)">
                Tamanho do sequencial{" "}
              </Form.Label>
              <Form.Control
                type="number"
                {...register("inputTamanhoSequencial", { required: true })}
                maxLength={1}
                defaultValue={field?.sequencialSize}
              />
            </Form.Group>
          )}

          {typeField === TypeFieldEnum.DATA_DINAMICA && (
            <Form.Group className="mb-3" controlId="formTipo">
              <Form.Label>Formato data/hora</Form.Label>
              <Select
                options={formatDateHourOptions}
                placeholder="Clique para selecionar"
                onChange={(e) => setFieldFormatDate(e?.value)}
                defaultValue={{
                  label: field?.formatDate,
                  value: field?.formatDate,
                }}
              />
            </Form.Group>
          )}

          {typeField === TypeFieldEnum.FILLER && (
            <Form.Group className="mb-3" controlId="formNome">
              <Form.Label>Tamanho do Filler </Form.Label>
              <Form.Control
                type="number"
                {...register("inputTamanhoFiller", { required: true })}
                defaultValue={field?.fillerSize}
                maxLength={5}
              />
            </Form.Group>
          )}

          {typeField === TypeFieldEnum.ABERTO && (
            <>
              <Form.Group className="mb-3" controlId="formTamanho">
                <Form.Label>Tamanho: </Form.Label>
                <Form.Control
                  type="number"
                  min="1"
                  {...register("inputTamanho", {
                    required: true,
                  })}
                  maxLength={30}
                  defaultValue={field?.size}
                />
                <div className="help-block with-errors"></div>
              </Form.Group>
              <Form.Group className="mb-3" controlId="formTipo">
                <Form.Label>Tipo: </Form.Label>
                <Select
                  options={typeFieldOptions}
                  placeholder="Clique para selecionar"
                  defaultValue={{
                    label: field?.type,
                    value: field?.type,
                  }}
                  onChange={(e) => setFieldFormatDate(e?.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formObrigatorio">
                <Form.Label>Obrigatório?</Form.Label>
                <Select
                  options={typeBooleanOptions}
                  placeholder="Clique para selecionar"
                  defaultValue={{
                    label: field?.mandatory,
                    value: field?.mandatory,
                  }}
                  onChange={(e) => setFieldMandatory(e?.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formMandatory">
                <Form.Label>O valor é fixo?</Form.Label>
                <Select
                  options={typeBooleanOptions}
                  placeholder="Clique para selecionar"
                  onChange={(e) => setFieldFixed(e?.value)}
                  defaultValue={{
                    label: field?.fixed,
                    value: field?.fixed,
                  }}
                />
              </Form.Group>
              {fieldFixed === "SIM" && (
                <Form.Group className="mb-3" controlId="formPadrao">
                  <Form.Label>Valor padrão </Form.Label>
                  <Form.Control
                    type="text"
                    {...register("inputPadrao", { required: true })}
                    maxLength={30}
                    defaultValue={field?.default}
                  />
                </Form.Group>
              )}

              {fieldFixed === "NAO" && (
                <Form.Group className="mb-3" controlId="formNumZendesk">
                  <Form.Label>Número do campo no zendesk </Form.Label>
                  <Form.Control
                    type="text"
                    {...register("inputCustomFieldNumber", {
                      required: true,
                    })}
                    maxLength={30}
                    defaultValue={field?.customFieldNumber}
                  />
                </Form.Group>
              )}
            </>
          )}

          <div className="btns-action">
            <Button variant="success" type="submit" className="btn-create">
              Salvar
            </Button>

            <Button
              variant="warning"
              onClick={cleanAndCloseModal}
              className="btn-exit"
            >
              Cancelar
            </Button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

export default ModalEditField;
