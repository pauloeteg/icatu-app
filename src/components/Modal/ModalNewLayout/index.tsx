import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { SubmitHandler, useForm } from "react-hook-form";
import { createLayout } from "../../../api/layout-api";
import { useHistory } from "react-router-dom";
import Select from "react-select";

import { Alert } from "../../Alert";

import "../styles.scss";
import { typeLayoutOptions } from "../../../models/ILayout";

interface IModal {
  show: boolean;
  handleClose: () => void;
  updatePage?: () => void;
}

type InputsProps = {
  inputNome?: string;
  inputType?: string;
};

const ModalNewLayout: React.FC<IModal> = ({ show, handleClose }) => {
  const history = useHistory();

  const [tipoLayout, setTipoLayout] = useState<undefined | string>(undefined);

  const onSubmit: SubmitHandler<InputsProps> = async (data) => {
    try {
      const layoutCreated = await createLayout({
        layoutJson: [],
        name: data.inputNome,
        type: tipoLayout,
      });

      history.push(`/layouts/${layoutCreated?.id}`);

      Alert.success(
        "Layout criado com sucesso.",
        cleanAndCloseModal,
        undefined
      );
    } catch (error: any) {
      return;
    }
  };

  const { register, handleSubmit, reset } = useForm();

  const cleanAndCloseModal = () => {
    reset();
    handleClose();
  };

  return (
    <Modal
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      animation={false}
    >
      <Modal.Header>
        <Modal.Title>Criar um novo Layout</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className="mb-3" controlId="formNome">
            <Form.Label>Nome do layout </Form.Label>
            <Form.Control
              type="text"
              {...register("inputNome", { required: true })}
              maxLength={30}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formLayout">
            <Form.Label>Tipo do Layout do arquivo</Form.Label>
            <Select
              options={typeLayoutOptions}
              placeholder="Clique para selecionar"
              onChange={(e) => setTipoLayout(e?.value)}
            />
          </Form.Group>

          <div className="btns-action">
            <Button variant="success" type="submit" className="btn-create">
              Criar
            </Button>

            <Button
              variant="warning"
              onClick={cleanAndCloseModal}
              className="btn-exit"
            >
              Sair
            </Button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

export default ModalNewLayout;
