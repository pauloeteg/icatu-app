import React, { SetStateAction, useEffect, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { SubmitHandler, useForm } from "react-hook-form";
import { updateJob } from "../../../api/job-api";
import { Alert } from "../../Alert";
import Select from "react-select";

import { IJob, optionsTime } from "../../../models/IJob";
import SpinnerDefault from "../../Spinner";
import { getLayouts } from "../../../api/layout-api";

import "../styles.scss";
import { InputSelectProps, InputsJobProps } from "../../../models/IInput";

interface IModal {
  show: boolean;
  job: IJob | undefined;
  handleClose: () => void;
  updatePage: () => void;
}

const ModalEditJob: React.FC<IModal> = ({
  show,
  handleClose,
  updatePage,
  job,
}) => {
  const [time, setTime] = useState<undefined | string>(undefined);

  const [isLoading, setIsLoading] = useState<boolean>(false);

  const [layout, setLayout] = useState<
    SetStateAction<undefined> | undefined | string
  >(undefined);

  const [optionsLayouts, setOptionsLayouts] = useState<InputSelectProps[]>([]);

  const getOptionsLayout = async () => {
    const jobs = await getLayouts();

    const optionsLayout: InputSelectProps[] = [];
    jobs.map((lyt) => optionsLayout.push({ value: lyt.id, label: lyt.name }));

    setOptionsLayouts(optionsLayout);
  };

  useEffect(() => {
    getOptionsLayout();
  }, []);

  const onSubmit: SubmitHandler<InputsJobProps> = async (data) => {
    try {
      setIsLoading(true);

      await updateJob({
        id: job?.id,
        name: data.inputName,
        customer: data.inputCustomer,
        startDate: data.inputStartDate,
        time: time,
        layout: layout,
      });
      setIsLoading(false);

      Alert.success(
        "Job atualizado com sucesso.",
        cleanAndCloseModal,
        undefined
      );
    } catch (error: any) {
      Alert.error(error?.response?.data?.message, undefined);
    }
  };

  const { register, handleSubmit, reset } = useForm();

  const cleanAndCloseModal = () => {
    setTime(undefined);
    reset();
    handleClose();
    updatePage();
  };

  return (
    <Modal
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      animation={false}
    >
      <Modal.Header>
        <Modal.Title>Editar job</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className="mb-3" controlId="formName">
            <Form.Label>Nome</Form.Label>
            <Form.Control
              type="text"
              {...register("inputName", { required: true })}
              defaultValue={job?.name}
              maxLength={30}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formTime">
            <Form.Label>Hora</Form.Label>
            <Select
              options={optionsTime}
              placeholder="Clique para selecionar"
              onChange={(e) => setTime(e?.value)}
              defaultInputValue={job?.time + ":00"}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formCustomer">
            <Form.Label>Cliente</Form.Label>
            <Form.Control
              type="text"
              {...register("inputCustomer", {
                required: true,
              })}
              defaultValue={job?.customer}
              maxLength={30}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formFile">
            <Form.Label>Tipo do Layout do arquivo</Form.Label>
            <Select
              options={optionsLayouts}
              placeholder="Clique para selecionar"
              onChange={(e) => setLayout(e?.value)}
              defaultInputValue={job?.layout?.name}
            />
          </Form.Group>

          <div className="btns-action">
            <Button
              variant="success"
              type="submit"
              className="btn-create"
              disabled={isLoading}
            >
              Salvar
            </Button>

            <Button
              variant="warning"
              onClick={cleanAndCloseModal}
              className="btn-exit"
              disabled={isLoading}
            >
              Cancelar
            </Button>
          </div>
        </Form>
        {isLoading && <SpinnerDefault />}
      </Modal.Body>
    </Modal>
  );
};

export default ModalEditJob;
