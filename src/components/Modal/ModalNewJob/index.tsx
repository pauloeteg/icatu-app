import React, { SetStateAction, useEffect, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { SubmitHandler, useForm } from "react-hook-form";
import Select from "react-select";

import { createJob } from "../../../api/job-api";
import { Alert } from "../../Alert";
import SpinnerDefault from "../../Spinner";

import { getLayouts } from "../../../api/layout-api";
import { optionsTime } from "../../../models/IJob";
import { InputSelectProps, InputsJobProps } from "../../../models/IInput";

interface IModal {
  show: boolean;
  handleClose: () => void;
  updatePage: () => void;
}

const ModalNewJob: React.FC<IModal> = ({ show, handleClose, updatePage }) => {
  const [time, setTime] = useState<
    SetStateAction<undefined> | undefined | string
  >(undefined);

  const [layout, setLayout] = useState<SetStateAction<undefined> | string>(
    undefined
  );

  const [optionsLayouts, setOptionsLayouts] = useState<InputSelectProps[]>([]);

  const getOptionsLayout = async () => {
    const jobs = await getLayouts();

    const optionsLayout: InputSelectProps[] = [];
    jobs.map((lyt) => optionsLayout.push({ value: lyt.id, label: lyt.name }));

    setOptionsLayouts(optionsLayout);
  };

  useEffect(() => {
    getOptionsLayout();
  }, []);

  const [isLoading, setIsLoading] = useState<boolean>(false);

  const onSubmit: SubmitHandler<InputsJobProps> = async (data) => {
    try {
      setIsLoading(true);
      await createJob({
        name: data.inputName,
        customer: data.inputCustomer,
        startDate: data.inputStartDate,
        time: time,
        layout: layout,
      });
      setIsLoading(false);

      Alert.success("Job criado com sucesso.", cleanAndCloseModal, undefined);
    } catch (error: any) {
      return;
    }
  };

  const { register, handleSubmit, reset } = useForm();

  const cleanAndCloseModal = () => {
    setTime(undefined);
    reset();
    handleClose();
    updatePage();
  };

  return (
    <Modal
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      animation={false}
    >
      <Modal.Header>
        <Modal.Title>Criar um novo Job</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className="mb-3" controlId="formName">
            <Form.Label>Nome do serviço</Form.Label>
            <Form.Control
              type="text"
              {...register("inputName", { required: true })}
              maxLength={30}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formCustomer">
            <Form.Label>Cliente</Form.Label>
            <Form.Control
              type="text"
              {...register("inputCustomer", {
                required: true,
              })}
              maxLength={30}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formTime">
            <Form.Label>Hora</Form.Label>
            <Select
              options={optionsTime}
              placeholder="Clique para selecionar"
              onChange={(e) => setTime(e?.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formFile">
            <Form.Label>Layout do arquivo</Form.Label>
            <Select
              options={optionsLayouts}
              placeholder="Clique para selecionar"
              onChange={(e) => setLayout(e?.value)}
            />
          </Form.Group>
          <div className="btns-action">
            <Button
              variant="success"
              type="submit"
              className="btn-create"
              disabled={isLoading}
            >
              Criar
            </Button>

            <Button
              variant="warning"
              onClick={cleanAndCloseModal}
              className="btn-exit"
              disabled={isLoading}
            >
              Sair
            </Button>
          </div>
        </Form>
        {isLoading && <SpinnerDefault />}
      </Modal.Body>
    </Modal>
  );
};

export default ModalNewJob;
