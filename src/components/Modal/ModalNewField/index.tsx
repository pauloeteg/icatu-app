import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { SubmitHandler, useForm } from "react-hook-form";
import { insertField } from "../../../api/layout-api";
import { InputsFieldProps } from "../../../models/IInput";
import Select from "react-select";

import { Alert } from "../../Alert";

import "../styles.scss";
import {
  typeBooleanOptions,
  typeFieldOptions,
  typeFieldBooleanOptions,
  formatDateHourOptions,
  TypeFieldEnum,
} from "../../../models/ILayout";

interface IModal {
  show: boolean;
  handleClose: () => void;
  updatePage: () => void;
  idLayout: string | undefined;
}

const ModalNewField: React.FC<IModal> = ({
  show,
  handleClose,
  updatePage,
  idLayout,
}) => {
  const [isMantatory, setIsMantatory] = useState<undefined | string>(undefined);
  const [fieldType, setFieldType] = useState<undefined | string>(undefined);
  const [isFixed, setIsFixed] = useState<undefined | string>(undefined);
  const [typeField, setTypeField] = useState<undefined | TypeFieldEnum>(
    undefined
  );
  const [formatDate, setFieldFormatDate] = useState<undefined | string>(
    undefined
  );
  const onSubmit: SubmitHandler<InputsFieldProps> = async (data) => {
    if (!typeField || !data.inputNome) {
      alert("Tipo e nome do campo é obrigatório.");
      return;
    }
    try {
      await insertField(idLayout, {
        name: data.inputNome,
        size: data.inputTamanho,
        type: fieldType,
        mandatory: isMantatory,
        default: data.inputPadrao,
        fixed: isFixed,
        customFieldNumber: data.inputCustomFieldNumber,
        typeField: typeField,
        fillerSize: data.inputTamanhoFiller,
        sequencialSize: data.inputTamanhoSequencial,
        formatDate: formatDate,
      });

      Alert.success("Field criado com sucesso.", cleanAndCloseModal, undefined);
    } catch (error: any) {
      return;
    }
  };

  const { register, handleSubmit, reset } = useForm();

  const cleanAndCloseModal = () => {
    reset();
    handleClose();
    updatePage();
  };

  return (
    <Modal
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      animation={false}
    >
      <Modal.Header>
        <Modal.Title>Criar um novo Campo</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className="mb-3" controlId="formNome">
            <Form.Label>Nome </Form.Label>
            <Form.Control
              type="text"
              {...register("inputNome", { required: true })}
              maxLength={30}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formObrigatorio">
            <Form.Label>Qual tipo do campo?</Form.Label>
            <Select
              options={typeFieldBooleanOptions}
              placeholder="Clique para selecionar"
              onChange={(e) => setTypeField(e?.value)}
            />
          </Form.Group>

          {typeField === "FILLER" && (
            <Form.Group className="mb-3" controlId="formNome">
              <Form.Label>Tamanho do Filler </Form.Label>
              <Form.Control
                type="number"
                {...register("inputTamanhoFiller", { required: true })}
                maxLength={5}
              />
            </Form.Group>
          )}

          {typeField === "SEQUENCIAL" && (
            <Form.Group className="mb-3" controlId="formNome">
              <Form.Label title="Example: (1 - 1), (2 - 01), (3 - 001)">
                Tamanho do sequencial
              </Form.Label>
              <Form.Control
                type="number"
                {...register("inputTamanhoSequencial", { required: true })}
                maxLength={1}
              />
            </Form.Group>
          )}

          {typeField === TypeFieldEnum.DATA_DINAMICA && (
            <Form.Group className="mb-3" controlId="formTipo">
              <Form.Label>Formato data/hora</Form.Label>
              <Select
                options={formatDateHourOptions}
                placeholder="Clique para selecionar"
                onChange={(e) => setFieldFormatDate(e?.value)}
              />
            </Form.Group>
          )}

          {typeField === "ABERTO" && (
            <>
              <Form.Group className="mb-3" controlId="formTamanho">
                <Form.Label>Tamanho </Form.Label>
                <Form.Control
                  type="number"
                  min="1"
                  {...register("inputTamanho", {
                    required: true,
                  })}
                  maxLength={30}
                />
                <div className="help-block with-errors"></div>
              </Form.Group>
              <Form.Group className="mb-3" controlId="formTipo">
                <Form.Label>Tipo </Form.Label>
                <Select
                  options={typeFieldOptions}
                  placeholder="Clique para selecionar"
                  onChange={(e) => setFieldType(e?.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formObrigatorio">
                <Form.Label>Obrigatório?</Form.Label>
                <Select
                  options={typeBooleanOptions}
                  placeholder="Clique para selecionar"
                  onChange={(e) => setIsMantatory(e?.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formMandatory">
                <Form.Label>O valor é fixo?</Form.Label>
                <Select
                  options={typeBooleanOptions}
                  placeholder="Clique para selecionar"
                  onChange={(e) => setIsFixed(e?.value)}
                />
              </Form.Group>

              {isFixed === "SIM" && (
                <Form.Group className="mb-3" controlId="formPadrao">
                  <Form.Label>Valor padrão </Form.Label>
                  <Form.Control
                    type="text"
                    {...register("inputPadrao", { required: true })}
                    maxLength={30}
                  />
                </Form.Group>
              )}

              {isFixed === "NAO" && (
                <Form.Group className="mb-3" controlId="formNumZendesk">
                  <Form.Label>Número do campo no zendesk </Form.Label>
                  <Form.Control
                    type="text"
                    {...register("inputCustomFieldNumber", { required: true })}
                    maxLength={30}
                  />
                </Form.Group>
              )}
            </>
          )}

          <div className="btns-action">
            <Button variant="success" type="submit" className="btn-create">
              Criar
            </Button>

            <Button
              variant="warning"
              onClick={cleanAndCloseModal}
              className="btn-exit"
            >
              Sair
            </Button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

export default ModalNewField;
