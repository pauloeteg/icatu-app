import React from "react";
import { Container, Navbar, Nav, NavDropdown } from "react-bootstrap";
import { FaUserAlt } from "react-icons/fa";
import { IoExitOutline } from "react-icons/io5";
import { NavLink, useHistory } from "react-router-dom";
import Cookies from "js-cookie";

import { Alert } from "../../components/Alert";

import "./styles.scss";

const NavDefault: React.FC = () => {
  const history = useHistory();

  const signOut = () => {
    Alert.success(
      "Logoff efetuado com sucesso.",
      () => {
        Cookies.remove("access_token");
        history.push("/");
      },
      undefined
    );
  };

  return (
    <Navbar className="blue-theme-nav" expand="md">
      <Container fluid="md">
        <NavLink className="navbar-brand" to="/jobs">
          Icatu App
        </NavLink>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />

        <Navbar.Collapse id="responsive-navbar-nav">
          <NavLink className="nav-link" to="/jobs">
            Jobs
          </NavLink>
          <NavLink className="nav-link" to="/layouts">
            Layouts
          </NavLink>
          <Nav className="me-auto" />
          <Nav>
            <NavDropdown
              title={<FaUserAlt color="white" />}
              id="basic-nav-dropdown"
            >
              <NavDropdown.Item onClick={signOut}>
                Sair <IoExitOutline color="black" />
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NavDefault;
