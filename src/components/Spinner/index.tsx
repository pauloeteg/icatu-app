import React from "react";
import { Spinner } from "react-bootstrap";
import "./styles.scss";

const SpinnerDefault: React.FC = () => {
  return (
    <div className="container">
      <Spinner
        animation="grow"
        variant="dark"
        as="span"
        role="status"
        className=""
      >
        <span className="visually-hidden">Loading...</span>
      </Spinner>
    </div>
  );
};

export default SpinnerDefault;
