export type InputSelectProps = {
  value: string | undefined;
  label: string | undefined;
};

export type InputsJobProps = {
  inputName?: string;
  inputTime?: number;
  inputStartDate?: string | undefined;
  inputCustomer?: number;
  inputFile?: number;
};

export type InputsFieldProps = {
  inputNome?: string;
  inputTipo?: string;
  inputTamanho?: string;
  inputObrigatorio?: string;
  inputPadrao?: string;
  inputFixo?: string;
  inputCustomFieldNumber?: string;
  inputIsNull: string;
  inputTamanhoFiller: string;
  inputTamanhoSequencial: string;
};
