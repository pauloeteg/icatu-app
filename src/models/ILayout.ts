export interface ILayout {
  id?: string;
  name?: string;
  type?: string;
  layoutJson?: IJson[];
  createdAt?: string;
}

export interface IJson {
  name: string | undefined;
  type: string | undefined;
  size: string | undefined;
  mandatory: string | undefined;
  fixed: string | undefined;
  default: string | undefined;
  customFieldNumber: string | undefined;
  typeField: TypeFieldEnum | undefined;
  fillerSize: string | undefined;
  sequencialSize: string | undefined;
  formatDate: string | undefined;
}

export enum TypeFieldEnum {
  ABERTO = "ABERTO",
  NULO = "NULO",
  FILLER = "FILLER",
  SEQUENCIAL = "SEQUENCIAL",
  DATA_DINAMICA = "DATA_DINAMICA",
  HEADER = "HEADER",
}

export const typeLayoutOptions = [
  { value: "VIDA", label: "VIDA" },
  { value: "CAP", label: "CAPITALIZAÇÃO" },
];

export const typeBooleanOptions = [
  { value: "SIM", label: "SIM" },
  { value: "NAO", label: "NAO" },
];

export const typeFieldBooleanOptions = [
  { value: TypeFieldEnum.ABERTO, label: TypeFieldEnum.ABERTO },
  { value: TypeFieldEnum.NULO, label: TypeFieldEnum.NULO },
  { value: TypeFieldEnum.FILLER, label: TypeFieldEnum.FILLER },
  { value: TypeFieldEnum.SEQUENCIAL, label: TypeFieldEnum.SEQUENCIAL },
  { value: TypeFieldEnum.DATA_DINAMICA, label: TypeFieldEnum.DATA_DINAMICA },
  { value: TypeFieldEnum.HEADER, label: TypeFieldEnum.HEADER },
];

export const typeFieldOptions = [
  { value: "NUM", label: "NUM" },
  { value: "ALFA", label: "ALFA" },
];

export const formatDateHourOptions = [
  { value: "ddMM", label: "ddMM" },
  { value: "ddMMyyyy", label: "ddMMyyyy" },
  { value: "hhmm", label: "hhmm" },
];
