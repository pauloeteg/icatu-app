import React, { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";

import { login } from "../../api/auth-api";
import { Alert } from "../../components/Alert";
import SpinnerDefault from "../../components/Spinner";

import "./styles.scss";

type Inputs = {
  inputEmail: string;
  inputPassword: string;
};

const Login: React.FC = () => {
  const history = useHistory();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();

  const [isLoading, setIsLoading] = useState<boolean>(false);

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    try {
      setIsLoading(true);
      await login({ email: data.inputEmail, password: data.inputPassword });
      setIsLoading(false);
      history.push("/jobs");
    } catch (error: any) {
      Alert.error(error?.response?.data?.message, undefined);
    }
  };

  return (
    <div className="all">
      <main className="container">
        <div className="div-login">
          <h2 className="title-login">Login</h2>
          <form className="form-login" onSubmit={handleSubmit(onSubmit)}>
            <div className="input-field">
              <input
                type="text"
                id="input-email"
                placeholder="Email"
                maxLength={100}
                {...register("inputEmail", {
                  required: true,
                  pattern: {
                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                    message: "Entre com um e-mail válido.",
                  },
                })}
              />
              {errors.inputEmail && (
                <span className="error-field">Entre com um e-mail válido.</span>
              )}
              <div className="underline" />
            </div>

            <div className="input-field">
              <input
                type="password"
                id="input-password"
                placeholder="Password"
                {...register("inputPassword", { required: true })}
              />
              {errors.inputPassword && (
                <span className="error-field">O password é obrigatório</span>
              )}

              <div className="underline" />
            </div>

            <button
              type="submit"
              value="Login"
              className="input-login"
              disabled={isLoading}
            >
              Login
            </button>
          </form>
          {isLoading && <SpinnerDefault />}
        </div>
      </main>
    </div>
  );
};
export default Login;
