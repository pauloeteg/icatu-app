import React, { useEffect, useState } from "react";
import { Button, Container, Table } from "react-bootstrap";
import { FaTrash } from "react-icons/fa";
import { IoMdAddCircle } from "react-icons/io";
import { AiFillEdit } from "react-icons/ai";

import ModalNewJob from "../../components/Modal/ModalNewJob";
import { getJobs, deleteJob, getOneJob } from "../../api/job-api";
import { IJob } from "../../models/IJob";
import NavDefault from "../../components/NavDefault";
import ModalEdit from "../../components/Modal/ModalEditJob";

import "./styles.scss";
import { DataTools } from "../../utils/dataTools";

const Jobs: React.FC = () => {
  const [showModalNew, setShowModalNew] = useState(false);
  const handleCloseModalNew = () => setShowModalNew(false);
  const handleShowModalNew = () => setShowModalNew(true);

  const [showModalEdit, setShowModalEdit] = useState(false);
  const handleCloseModalEdit = () => {
    setShowModalEdit(false);
  };
  const handleShowModalEdit = async (id: string | undefined) => {
    const jobLoadedDb = await getOneJob(id);
    setJobToEdit(jobLoadedDb);
    setShowModalEdit(true);
  };

  const [jobs, setJobs] = useState<IJob[]>([]);
  const [jobToEdit, setJobToEdit] = useState<IJob | undefined>(undefined);

  async function showAllJobs() {
    setJobs(await getJobs());
  }

  const removeJob = async (id: string | undefined) => {
    const according = window.confirm(
      "Tem certeza que deseja excluir esse job?"
    );

    if (according) {
      await deleteJob(id);
      showAllJobs();
    }
  };

  useEffect(() => {
    showAllJobs();
  }, []);

  return (
    <React.Fragment>
      <NavDefault />
      <div className="content-title">
        <h2 className="title">Gerenciador de jobs</h2>
        <Button className="btn-default" onClick={handleShowModalNew}>
          Novo Job <IoMdAddCircle />
        </Button>
      </div>

      <Container fluid="md">
        {jobs ? (
          <Table borderless hover className="table-default">
            <thead>
              <tr className="tr-thead">
                <th>Job</th>
                <th>Cliente</th>
                <th>Horário Programado</th>
                <th>Data de inicio</th>
                <th>Layout</th>
                <th>Ações</th>
              </tr>
            </thead>
            <tbody>
              {jobs &&
                jobs.map((item) => (
                  <tr key={item.id} className="tr-tbody">
                    <td children={item.name} />
                    <td children={item.customer} />
                    <td children={DataTools.addStrHrs(item.time)} />
                    <td children={DataTools.euaToBr(item.startDate)} />
                    <td children={item.layout?.name} />
                    <td className="icons">
                      <AiFillEdit
                        size={20}
                        className="icon-edit"
                        onClick={() => {
                          handleShowModalEdit(item?.id);
                        }}
                        title="Editar serviço"
                      />
                      <FaTrash
                        size={20}
                        className="icon-trash"
                        onClick={() => removeJob(item.id)}
                        title="Deletar serviço"
                      />
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        ) : (
          <div className="container">
            <span className="jobs-null-message">
              <hr />
              No momento, não existem registros cadastrados.
              <hr />
            </span>
          </div>
        )}
        {showModalNew && (
          <ModalNewJob
            show={showModalNew}
            handleClose={handleCloseModalNew}
            updatePage={showAllJobs}
          />
        )}
        {showModalEdit && (
          <ModalEdit
            show={showModalEdit}
            handleClose={handleCloseModalEdit}
            updatePage={showAllJobs}
            job={jobToEdit}
          />
        )}
      </Container>
    </React.Fragment>
  );
};

export default Jobs;
