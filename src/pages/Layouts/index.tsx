import React, { useEffect, useState } from "react";
import { Button, Container, Table } from "react-bootstrap";
import { FaTrash } from "react-icons/fa";
import { IoMdAddCircle } from "react-icons/io";
import { AiFillEdit } from "react-icons/ai";
import { HiDuplicate } from "react-icons/hi";

import NavDefault from "../../components/NavDefault";

import { ILayout } from "../../models/ILayout";
import {
  deleteLayout,
  duplicateLayout,
  getLayouts,
} from "../../api/layout-api";
import { useHistory } from "react-router-dom";
import { DataTools } from "../../utils/dataTools";
import ModalNewLayout from "../../components/Modal/ModalNewLayout";

const Layouts: React.FC = () => {
  const history = useHistory();

  const [showModalNew, setShowModalNew] = useState(false);
  const handleCloseModalNew = () => setShowModalNew(false);
  const handleShowModalNew = () => setShowModalNew(true);

  const [layouts, setLayouts] = useState<ILayout[]>([]);

  const removeLayout = async (id: string | undefined) => {
    const according = window.confirm(
      "Tem certeza que deseja excluir esse layout?"
    );

    if (according) {
      await deleteLayout(id);
      showAllLayouts();
    }
  };

  const copyPasteLayout = async (id: string | undefined) => {
    const according = window.confirm(
      "Tem certeza que deseja duplicar esse layout?"
    );

    if (according) {
      await duplicateLayout(id);
      showAllLayouts();
    }
  };

  const showAllLayouts = async () => {
    const layouts = await getLayouts();
    setLayouts(layouts);
  };

  useEffect(() => {
    showAllLayouts();
  }, []);

  return (
    <React.Fragment>
      <NavDefault />

      <div className="content-title">
        <h2 className="title">Gerenciar layouts de arquivos de proposta</h2>
        <Button className="btn-default" onClick={handleShowModalNew}>
          Novo Layout <IoMdAddCircle />
        </Button>
      </div>

      <Container fluid="md">
        {layouts ? (
          <Table borderless hover className="table-default">
            <thead>
              <tr className="tr-thead">
                <th>Nome do layout</th>
                <th>Tipo do layout</th>
                <th>Data criação</th>
                <th>Ações</th>
              </tr>
            </thead>
            <tbody>
              {layouts &&
                layouts.map((item) => (
                  <tr
                    key={item.id}
                    className="tr-tbody"
                    onDoubleClick={() => history.push(`/layouts/${item.id}`)}
                    style={{ cursor: "pointer" }}
                  >
                    <td children={item.name} />
                    <td children={item.type} />
                    <td children={DataTools.euaToBr(item.createdAt)} />
                    <td className="icons">
                      <AiFillEdit
                        size={20}
                        className="icon-edit"
                        title="Editar layout"
                        onClick={() => history.replace(`/layouts/${item.id}`)}
                      />
                      <FaTrash
                        size={20}
                        className="icon-trash"
                        onClick={() => removeLayout(item.id)}
                        title="Deletar layout"
                      />

                      <HiDuplicate
                        size={20}
                        className="icon-duplicate"
                        onClick={() => copyPasteLayout(item.id)}
                        title="Duplicar layout"
                      />
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        ) : (
          <div className="container">
            <span className="null-message">
              <hr />
              No momento, não existem registros cadastrados.
              <hr />
            </span>
          </div>
        )}

        {showModalNew && (
          <ModalNewLayout
            show={showModalNew}
            handleClose={handleCloseModalNew}
          />
        )}
      </Container>
    </React.Fragment>
  );
};

export default Layouts;
