import React from "react";
import { Table } from "react-bootstrap";
import { ILayout } from "../../../models/ILayout";
import TrDraggable from "../TrDraggable";

interface LayoutTableProps {
  layout: ILayout;
  handleShowModalEdit: any;
  loadLayout: any;
}

const LayoutTable: React.FC<LayoutTableProps> = ({
  layout,
  handleShowModalEdit,
  loadLayout,
}) => {
  return (
    <Table borderless hover className="table-default">
      <thead>
        <tr className="tr-thead">
          <th>Posição</th>
          <th>Nome do Campo</th>
          <th>Tipo do valor</th>
          <th>Tamanho</th>
          <th>Obrigatório</th>
          <th>Fixo</th>
          <th>Valor padrão</th>
          <th>Custom field</th>
          <th>Classe do campo</th>
          <th>Ações</th>
        </tr>
      </thead>
      <tbody>
        {layout?.layoutJson &&
          layout?.layoutJson.map((field, index) => (
            <TrDraggable
              field={field}
              handleShowModalEdit={handleShowModalEdit}
              position={index}
              key={index}
              layout={layout}
              loadLayout={loadLayout}
            />
          ))}
      </tbody>
    </Table>
  );
};

export default LayoutTable;
