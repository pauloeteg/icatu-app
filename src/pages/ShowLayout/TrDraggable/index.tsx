import React, { useRef } from "react";
import { FaTrash } from "react-icons/fa";
import { AiFillEdit } from "react-icons/ai";
import { useDrag, useDrop } from "react-dnd";
import { IJson, ILayout } from "../../../models/ILayout";
import { deleteField, updatePositionField } from "../../../api/layout-api";

interface TrDraggableProps {
  position: number;
  field: IJson;
  handleShowModalEdit: any;
  layout: ILayout;
  loadLayout: any;
}

const TrDraggable: React.FC<TrDraggableProps> = ({
  position,
  field,
  handleShowModalEdit,
  layout,
  loadLayout,
}) => {
  const ref = useRef<any>();

  const [{ isDragging }, dragRef] = useDrag({
    item: { type: "CARD", id: position },
    type: "ROW",
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const moveFieldPosition = async (layout: any, item: any, position: any) => {
    if (item.id !== position) {
      const according = window.confirm(
        "Tem certeza que deseja mover esse field?"
      );

      if (according) {
        await updatePositionField(layout?.id, item.id, position);
        loadLayout();
      }
    }
  };

  const [, dropRef] = useDrop({
    accept: "ROW",
    drop(item: any, monitor) {
      moveFieldPosition(layout, item, position);
    },
  });

  const removeField = async (
    id: string | undefined,
    posField: string | undefined
  ) => {
    const according = window.confirm(
      "Tem certeza que deseja excluir esse field?"
    );

    if (according) {
      await deleteField(id, posField);
      loadLayout();
    }
  };

  dragRef(dropRef(ref));

  return (
    <tr
      id={"field" + position.toString()}
      key={position}
      className={`tr-tbody dragging-${isDragging}`}
      ref={ref}
    >
      <td children={position + 1} />
      <td children={field.name ? field.name : "--"} />
      <td children={field.type ? field.type : "--"} />
      <td children={field.size ? field.size : "--"} />
      <td children={field.mandatory ? field.mandatory : "--"} />
      <td children={field.fixed ? field.fixed : "--"} />
      <td children={field.default ? field.default : "--"} />
      <td children={field.customFieldNumber ? field.customFieldNumber : "--"} />
      <td children={field.typeField ? field.typeField : "--"} />
      <td className="icons">
        <AiFillEdit
          size={20}
          className="icon-edit"
          title="Editar field"
          onClick={() => {
            handleShowModalEdit(field, position);
          }}
        />
        <FaTrash
          size={20}
          className="icon-trash"
          onClick={() => removeField(layout?.id, position.toString())}
          title="Deletar field"
        />
      </td>
    </tr>
  );
};

export default TrDraggable;
