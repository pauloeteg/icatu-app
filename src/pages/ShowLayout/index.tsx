import React, { useEffect, useState } from "react";
import { Button, Container } from "react-bootstrap";

import NavDefault from "../../components/NavDefault";

import { IJson, ILayout } from "../../models/ILayout";
import { getOneLayout, updateNameLayout } from "../../api/layout-api";
import { useParams } from "react-router-dom";
import { IoMdAddCircle } from "react-icons/io";
import ModalNewField from "../../components/Modal/ModalNewField";
import ModalEditField from "../../components/Modal/ModalEditField";

import "./styles.scss";
import LayoutTable from "./LayoutTable";
interface ShowLayoutProps {
  id: string;
}

const ShowLayout: React.FC = () => {
  /// modal new
  const [showModalNew, setShowModalNew] = useState(false);
  const handleCloseModalNew = () => setShowModalNew(false);
  const handleShowModalNew = () => setShowModalNew(true);

  /// modal edit
  const [showModalEdit, setShowModalEdit] = useState(false);
  const handleCloseModalEdit = () => setShowModalEdit(false);
  const handleShowModalEdit = (field: IJson, index: number) => {
    setLayoutJson(field);
    setShowModalEdit(true);
    setPosField(index);
  };

  const { id } = useParams<ShowLayoutProps>();

  const [layout, setLayout] = useState<ILayout | undefined>();
  const [layoutJson, setLayoutJson] = useState<IJson>();
  const [posField, setPosField] = useState<number | undefined>();

  async function loadLayout() {
    setLayout(await getOneLayout(id));
  }

  const updateName = async (
    id: string | undefined,
    name: string | undefined
  ) => {
    if (layout?.name !== name) {
      const according = window.confirm(
        "Tem certeza que deseja alterar o nome?"
      );

      if (according) {
        await updateNameLayout(id, name);
        loadLayout();
      }
    }
  };

  useEffect(
    () => {
      loadLayout();
    }, // eslint-disable-next-line
    []
  );

  return (
    <React.Fragment>
      <NavDefault />
      <div className="content-title">
        <input
          className="title-layout"
          defaultValue={layout?.name}
          onBlur={(e) => updateName(layout?.id, e?.target.value)}
        />

        <Button className="btn-default" onClick={handleShowModalNew}>
          Novo Field <IoMdAddCircle />
        </Button>
      </div>
      <Container fluid="md">
        {layout?.layoutJson && layout?.layoutJson.length > 0 ? (
          <LayoutTable
            layout={layout}
            handleShowModalEdit={handleShowModalEdit}
            loadLayout={loadLayout}
          />
        ) : (
          <div className="container">
            <span className="null-message">
              <hr />
              No momento, não existem registros cadastrados.
              <hr />
            </span>
          </div>
        )}
        {showModalNew && (
          <ModalNewField
            show={showModalNew}
            handleClose={handleCloseModalNew}
            updatePage={loadLayout}
            idLayout={id}
          />
        )}
        {showModalEdit && (
          <ModalEditField
            show={showModalEdit}
            handleClose={handleCloseModalEdit}
            updatePage={loadLayout}
            posField={posField}
            idLayout={id}
            field={layoutJson}
          />
        )}
      </Container>
    </React.Fragment>
  );
};

export default ShowLayout;
